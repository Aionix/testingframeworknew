package PageObjectMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Sergey on 05.09.2016.
 */
public class PaymentPage {
    WebDriver driver;
    public PaymentPage(WebDriver driver){ this.driver = driver; }

public void enterPaymentInfo(WebDriver driver) {
    WebElement country = (new WebDriverWait(driver, 30))
            .until(ExpectedConditions
                    .elementToBeClickable(By.id("select2-PaymentForm_country-container")));
    country.click();
    driver.findElement(By.className("select2-search__field")).sendKeys("uk", Keys.ENTER);
    driver.findElement(By.id("select2-PaymentForm_nationality-container")).click();
    driver.findElement(By.className("select2-search__field")).sendKeys("uk", Keys.ENTER);
    driver.findElement(By.id("PaymentForm_cardNumber")).clear();
    driver.findElement(By.id("PaymentForm_cardNumber")).sendKeys("3569");
    driver.findElement(By.id("PaymentForm_cardNumber")).click();
    driver.findElement(By.id("PaymentForm_cardNumber")).sendKeys("9900");
    driver.findElement(By.id("PaymentForm_cardNumber")).click();
    driver.findElement(By.id("PaymentForm_cardNumber")).sendKeys("0000");
    driver.findElement(By.id("PaymentForm_cardNumber")).click();
    driver.findElement(By.id("PaymentForm_cardNumber")).sendKeys("0157");
    driver.findElement(By.id("select2-PaymentForm_expiryMonth-container")).click();
    driver.findElement(By.xpath("//li[text()='10']")).click();  //month
    driver.findElement(By.id("select2-PaymentForm_expiryYear-container")).click();
    driver.findElement(By.xpath("//li[text()='16']")).click();  //year
    driver.findElement(By.id("PaymentForm_cardCvx")).clear();
    driver.findElement(By.id("PaymentForm_cardCvx")).sendKeys("123"); //cvv
    driver.findElement(By.id("PaymentForm_firstName")).clear();
    driver.findElement(By.id("PaymentForm_firstName")).sendKeys("artyomTest"); //name
    driver.findElement(By.id("PaymentForm_lastName")).clear();
    driver.findElement(By.id("PaymentForm_lastName")).sendKeys("artyomBtest"); //lastname
    driver.findElement(By.xpath("//ul/li[2]/label/span")).click();

    driver.findElement(By.id("submit-button")).click();
    WebElement upcomingTrips = (new WebDriverWait(driver, 20))
            .until(ExpectedConditions
            .visibilityOfElementLocated(By.id("upcomingTrips")));
    }




    public String getSubtotal(WebDriver driver){
        return driver.findElement(By.xpath("//div[3]/div[1]/div[2]/span")).getText();
    }
    public String getFee(WebDriver driver){
        return driver.findElement(By.xpath("//div[3]/div[2]/div[2]/span")).getText();
    }
    public String getTotal(WebDriver driver){
        return driver.findElement(By.xpath("//div[4]//div[2]/span ")).getText();
    }


}


