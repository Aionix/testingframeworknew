package PageObjectMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Sergey on 23.08.2016.
 */
public class UpToDirectPage {
    WebDriver driver;
     public UpToDirectPage(WebDriver driver){ this.driver = driver; }

    public void enterDirectParamsUpTo(WebDriver driver, String length, String beam, String draught){

        driver.findElement(By.id("Length")).sendKeys(length);
        driver.findElement(By.id("Beam")).sendKeys(beam);
        driver.findElement(By.id("Draught")).sendKeys(draught);
        driver.findElement(By.cssSelector("#BoatType>option[value='3']")).click();
        driver.findElement(By.id("nbb-searchButton")).click();
        driver.findElement(By.cssSelector(".fa.fa-plus.mr-booking-radio")).click();
    }
    public void clickBookNow(WebDriver driver){
        driver.findElement(By.id("nbb-btn-real")).click(); //book now
    }

    public String getBerthPrice(WebDriver driver){
        return driver.findElement(By.className("place_price")).getText();
    }
    public String getServiceFee(WebDriver driver){
        return driver.findElement(By.className("service-fee-display")).getText();
    }
    public void getTotalDirectUpTo(WebDriver driver){
        driver.findElement(By.className("booking_total_price.h4")).getText();

}
}
