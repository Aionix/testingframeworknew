package PageObjectMethods;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

/**
 * Created by Sergey on 22.08.2016.
 */
public class MainPage {
    WebDriver driver;
    public MainPage(WebDriver driver){ this.driver = driver; }
    String email = "aionix@yandex.ua";
    String password = "123";

    public void loginFrontEnd(WebDriver driver) {
        WebElement loginButton = (new WebDriverWait(driver, 15))
                .until(elementToBeClickable(By.cssSelector("a[id='sign_in']")));
        loginButton.click();
        WebElement loginModal = (new WebDriverWait(driver, 15))
                .until(visibilityOfElementLocated(By.id("float_tabs")));
        WebElement loginField = (new WebDriverWait(driver, 15))
                .until(visibilityOfElementLocated(By.id("LoginForm_username")));
        loginField.sendKeys(email);
                driver.findElement(By.id("LoginForm_password")).sendKeys(password);
                driver.findElement(By.id("myButton1")).click();
        Boolean loginModalDisappear = (new WebDriverWait(driver, 30))
                .until(invisibilityOfElementLocated(By.id("float_tabs")));
    }

    public void searchMarina(WebDriver driver, String marinaName)  {
        WebElement marField = (new WebDriverWait(driver, 15))
                .until(elementToBeClickable(By.id("SearchForm_Destination")));
        marField.click();
        marField.sendKeys(marinaName);

        WebElement dropDown = (new WebDriverWait(driver, 10))
                .until(elementToBeClickable(By.xpath("//*[@id='ui-id-3']/li[1]")));
        dropDown.click();
        driver.findElement(By.id("home-search-form-search")).click();
        WebElement marPage = (new WebDriverWait(driver,15))
                .until(visibilityOfElementLocated(By.className("breadcrumb")));
        }


       // driver.findElement(By.xpath("//.[@id='ui-id-3']/li[1]")).click();
       // driver.findElement(By.xpath("html/body/ul/li[1]")).click();
       // driver.findElement(By.xpath("//.[contains(text(),'marinaName')]")).click();
       // driver.findElement(By.id("home-search-form-search")).click();

    }






