package PageObjectTests;

import PageObjectMethods.MainPage;
import PageObjectMethods.PaymentPage;
import PageObjectMethods.UpToDirectPage;
import Utils.BrowserSettingsBuy;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by Sergey on 23.08.2016.
 */
public class BuyUptoDirectTest extends BrowserSettingsBuy {
    MainPage mainPage = new MainPage(driver);
    UpToDirectPage upToPage = new UpToDirectPage(driver);
    PaymentPage paymentPage = new PaymentPage(driver);

    @Test
    public void buyUptoDirectAdded100() throws InterruptedException {
        mainPage.loginFrontEnd(driver);
        mainPage.searchMarina(driver, "UpToDirectAdded100");
        upToPage.enterDirectParamsUpTo(driver, "3", "3", "3");
        Assert.assertEquals("5.2  €", upToPage.getServiceFee(driver));
        upToPage.clickBookNow(driver);
        Assert.assertEquals("24.4 €", paymentPage.getSubtotal(driver));
        Assert.assertEquals("4.60 €", paymentPage.getFee(driver));
        Assert.assertEquals("29.00 €", paymentPage.getTotal(driver));
        paymentPage.enterPaymentInfo(driver);
    }
        }





