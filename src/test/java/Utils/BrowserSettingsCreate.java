package Utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sergey on 22.08.2016.
 */
public class BrowserSettingsCreate {
    WebDriver driver;


    @Before
            public void chromeStarter() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        ChromeOptions option = new ChromeOptions();
        option.addArguments("--start-maximized");
        WebDriver driver = new ChromeDriver(option);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        //
        driver.get("http://elearn.hopto.org:8070/midend/user/register");
        driver.findElement(By.cssSelector("div a[href*='authenticate']")).click();
        driver.findElement(By.cssSelector("#LoginForm_username")).sendKeys("aionix14@yandex.ua");
        driver.findElement(By.cssSelector("#LoginForm_password")).sendKeys("123");
        driver.findElement(By.cssSelector(".btn.btn-primary")).click();//sign in

    }
    @After
    public void closing(){
        driver.quit();
    }
}
