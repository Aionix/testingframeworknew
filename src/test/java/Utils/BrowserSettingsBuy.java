package Utils;


import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sergey on 23.08.2016.
 */
public abstract class BrowserSettingsBuy {
    public WebDriver driver;

    @Before
    public void chromeStarter() throws InterruptedException {
            System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
            ChromeOptions option = new ChromeOptions();
            option.addArguments("--start-maximized");
            driver = new ChromeDriver(option);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get("http://elearn.hopto.org:8080/");

    }

    @After
    public void close(){
        driver.quit();
    }
}
